//LANG:ko:한국어
// The line above marks the language
function makeStrings() {
        window.locale.appname = 'WebTunes';
        window.locale.vername = 'beta';
        window.locale.credit = 'WebTunes팀한역/AlsaLisa';
        
        // --------- Error and messages
        window.locale.errors.guestUpload = '파일을 업로드하려면 로그인을 해야  합니다.';
        window.locale.errors.uploadSuccess = '업로드 완료';
        window.locale.errors.uploadFail = '업로드 에러';
        window.locale.errors.login = ' 로그인을 해야 합니다. 로그인 페이지로 이동하려면 OK를 누르세요.';

        window.locale.errors.albumSaved = "앨범이 저장되었습니다.";
        window.locale.errors.linkError = '앨범 또는 카테고리를 제외한 다른 곳의 링크를 만들 수 없습니다.';
        window.locale.errors.saved = '저장되었습니다.';
        window.locale.errors.uploading = '업로드가 시작됩니다';
        window.locale.errors.notYourList = '다른 사람의 재생 목록을 편집할 수 없습니다.';
        window.locale.errors.fail = "에러";
        window.locale.errors.queueIsUndefined = '줄이 있지 않습니다(??)';
        window.locale.errors.playError = '재생 오류';
        window.locale.errors.deleted = '삭제되었습니다.';
        window.locale.errors.DLStart = "다운로드 시작";
        window.locale.errors.DLStartSC = "다운로드가 시작됩니다.. ";
        
        // --------- UI strings
        window.locale.ui.uploading = "업로드...";
        window.locale.ui.wait = '잠시 기다려주세요...';
        window.locale.ui.cloudStream = "사운드 클라우드 버퍼링...";
        window.locale.ui.buffering = "버퍼링...";
        window.locale.ui.bufferingStalled = "무한 버퍼링이 생긴 것 같습니다. ";
        window.locale.ui.soundCloudBeta = "SoundCloud 사운드 클라우드 베타";
        window.locale.ui.onSC = "SoundCloud에서";
        window.locale.ui.diskSpace = "빈 공간: ";
        window.locale.ui.artist = '아티스트';
        window.locale.ui.album = '앨범';
        window.locale.ui.compilation = '컴필레이션';
        window.locale.ui.songNameUnkn = '알 수 없는 노래';
        window.locale.ui.songBandUnkn = '알 수 없는 아티스트';
        window.locale.ui.songAlbumUnkn= '알 수 없는 앨범';
        window.locale.ui.shareTune = '트랙 공유';
        window.locale.ui.keepTune = '트랙 다운로드';
        window.locale.ui.usercontrol = '이용자 편집';
        window.locale.ui.linkImport = '심볼릭 가져오기';
        window.locale.ui.linkTrack = '트랙 심볼릭';
        window.locale.ui.searchCloud = 'SoundCloud 검색';
        window.locale.ui.cloudFeed = '새 음악';
        window.locale.ui.cloudFavs = '즐겨찾기';
        window.locale.ui.cloudOwn = '다운로드된 파일';
        window.locale.ui.cloudSearch = '찾기';
        window.locale.ui.about = "정보";
        window.locale.ui.shuffle = "무작위 설정";
        window.locale.ui.logon = "로그온";
        window.locale.ui.logoff= "로그오프";
        window.locale.ui.rename = "이름 바꾸기";
        window.locale.ui.share = "공유";
        window.locale.ui.linkMake = "Link creation in progress...";

        // --------- Dialog strings
        window.locale.dialogs.albumSave = "앨범 저장";
        window.locale.dialogs.albumDiscard = "앨범 삭제";
        window.locale.dialogs.linkTrack = "트랙 저장";
        window.locale.dialogs.cancel = "취소";
        window.locale.dialogs.plsRename = '이 재생 목록의 이름을 치세요.';
        window.locale.dialogs.copyLink = '재생 목록을 공유하려면 링크를 복사하세요. 받는 사람은 그냥 들을 수도 있고, 계정이 있을 경우 복사할 수도 있습니다.';
        window.locale.dialogs.copyLinkTrk = '트렉을 공유하려면 링크를 복사하세요. 받는 사람은 재생 목록/앨범에 액세스할 수 있습니다.';
        window.locale.dialogs.trash = '쓰레기통. 삭제하려면 여기로 드래그하세요.';
        
        // --- Multipart String 'Really delete playlist NAME? To delete....'
        window.locale.dialogs.del1 = '재생 목록을 삭제하시겠습니까?«';
        window.locale.dialogs.del2 = '»? 삭제하려면 휴지통으로 드래그하세요.';
        // --- End multipart string
        
        window.locale.dialogs.newList = '새 플레리리스트 이름을 치세요.';
        window.locale.dialogs.unamedList = '가장 좋아하는 음악';
        
        window.locale.dialogs.newSClist = 'SoundCloud 사운드 클라우드의 새 세트의 이름을 치세요.';
        window.locale.dialogs.unamedSClist = '가장 좋아하는 음악 '+window.locale.appname;
        
        
        // --------- Sidebar strings
        window.locale.sidebar.library = "보관함";
        window.locale.sidebar.music = "음악";
        window.locale.sidebar.compilations = "컴필레이션";
        window.locale.sidebar.unsorted = "분류되지 않은";
        window.locale.sidebar.playlists = "재생 목록";
        window.locale.sidebar.soundcloud = 'SoundCloud';
        window.locale.sidebar.albums = '앨범';
        
        window.locale.ui.vkOwn = "내 음악";
        window.locale.ui.searchVK = "VK.com 음악검색";
        window.locale.sidebar.vk = "vk.com";

                window.locale.sidebar.radio = '라디오';
        window.locale.sidebar.mystations = '내 라디오 방송국';
        window.locale.sidebar.catalog = '방송국 카탈로그';
        window.locale.radio.stationName = "새 방송국의 이름은?";
        window.locale.radio.stationAddress = "새 방송국주소?";
}