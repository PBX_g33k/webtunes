//LANG:ar:Arabic
function makeStrings() {
		window.locale.appname = 'WebTunes';
		window.locale.vername = 'beta';
		window.locale.credit = 'Arabic Translation by @ij0s3ph';

		// --------- Error and messages
		window.locale.errors.guestUpload = 'رفع الملفات غير مسموح للزوار';
		window.locale.errors.uploadSuccess = 'تمت عملية الرفع بنجاح';
		window.locale.errors.uploadFail = 'فشلة عملية الرفع';
		window.locale.errors.login = 'النظام يتطلب تسجيل دخول أو دعوة خاصه للالبوم، إضغط موافق لتسجيل الدخول.';
		window.locale.errors.albumSaved = "تم حفظ الألبوم";
		window.locale.errors.linkError = 'لا يمكن عمل إختصار بدون ألبوم أو فئه';
		window.locale.errors.saved = 'تم الحفظ';
		window.locale.errors.uploading = 'جاري الرفع';
		window.locale.errors.notYourList = 'لا تستطيع تعديل لائحة تشغيل مستخدم آخر';
		window.locale.errors.fail = "فشل";
		window.locale.errors.queueIsUndefined = 'القائمه غير موجودة';
		window.locale.errors.playError = 'خطأ في التشغيل';
		window.locale.errors.deleted = 'تم الحذف';
		window.locale.errors.DLStart = "جاري التحميل";
		window.locale.errors.DLStartSC = "جاري التحميل, الرجاء الإنتظار.. (تحميلات SoundCloud مازالت تجريبيه)";

		// --------- UI strings
		window.locale.ui.uploading = "جاري الرفع...";
		window.locale.ui.wait = 'الرجاء الإنتظار...';
		window.locale.ui.cloudStream = "التحميل المسبق من SoundCloud  (ممكن أن تكون غير دقيقة)";
		window.locale.ui.buffering = "جاري التخزين الؤقت...";
		window.locale.ui.bufferingStalled = "يبدو أن التخزين المؤقت متوقف...";
		window.locale.ui.soundCloudBeta = "مازال تجريبي SoundCloud الدمج في";
		window.locale.ui.onSC = "SoundCloud في";
		window.locale.ui.diskSpace = "مساحة القرص: ";
		window.locale.ui.artist = 'الفنان';
		window.locale.ui.album = 'الألبوم';
		window.locale.ui.compilation = 'مجموعة';
		window.locale.ui.songNameUnkn = 'إسم الأغنيه غير معروف';
		window.locale.ui.songBandUnkn = 'الفنان غير معروف';
		window.locale.ui.songAlbumUnkn= 'الألبوم غير معروف';
		window.locale.ui.shareTune = 'مشاركة الأغنية';
		window.locale.ui.keepTune = 'تحميل الأغنية';
		window.locale.ui.usercontrol = 'تعديل المستخدمين';
		window.locale.ui.linkImport = 'إستيراد الإختصارات';
		window.locale.ui.linkTrack = 'إنشاء إختصار للأغنية';
		window.locale.ui.searchCloud = 'SoundCloud البحث في';
		window.locale.ui.cloudFeed = 'شريط الخلاصات';
		window.locale.ui.cloudFavs = 'المفضلة';
		window.locale.ui.cloudOwn = 'الملفات المرفوعه';
		window.locale.ui.cloudSearch = 'بحث';
		window.locale.ui.about = "حول";
		window.locale.ui.shuffle = "عشوائي";
		window.locale.ui.logon = "تسجيل الدخول";
		window.locale.ui.logoff= "تسجيل الخروج";
		window.locale.ui.rename = "إعادة تسميه";
		window.locale.ui.share = "مشاركة";
		window.locale.ui.linkMake = "Making link, please wait...";

		// --------- Dialog strings
		window.locale.dialogs.albumSave = "حفظ الألبوم";
		window.locale.dialogs.albumDiscard = "إلغاء الألبوم";
		window.locale.dialogs.linkTrack = "حفظ الأغنية";
		window.locale.dialogs.cancel = "إلغاء";
		window.locale.dialogs.plsRename = 'أدخل إسم قائمة التشغيل';
		window.locale.dialogs.copyLink = 'إنسخ الرابط لمشاركة قائمة التشغيل، يمكن للمستخدم الإستماع ونسخ قائمة التشغيل إلى مجموعته';
		window.locale.dialogs.copyLinkTrk = 'إنسخ الرابط لمشاركة الأغنية الحاليه، يمكن للمستخدم مشاهدة قائمة التشغيل أو الألبوم الذي يحتوي على هذه الأغنية.';
		window.locale.dialogs.trash = 'إسحب الأغاني إلى هنا للحذف';

		// --- Multipart String 'Really delete playlist NAME? To delete....'
		window.locale.dialogs.del1 = 'تآكيد حذف قائمة التشغيل';
		window.locale.dialogs.del2 = 'لحذف أغنية من القائمة، يرجى سحبها إلى أيقونة سلة المهملات؟';
		// --- End multipart string

		window.locale.dialogs.newList = 'إسم قائمة التشغيل الجديدة؟';
		window.locale.dialogs.unamedList = 'قائمة بدون إسم';

		window.locale.dialogs.newSClist = 'جديدة؟ SoundCloud إسم قائمة تشغيل';
		window.locale.dialogs.unamedSClist = 'WebTunes قائمة تشغيل';


		// --------- Sidebar strings
		window.locale.sidebar.library = "المكتبة";
		window.locale.sidebar.music = "موسيقى";
		window.locale.sidebar.compilations = "مجموعات";
		window.locale.sidebar.unsorted = "أغاني غير مصتفه";
		window.locale.sidebar.playlists = "قوائم التشغيل";
		window.locale.sidebar.soundcloud = 'SoundCloud';
		window.locale.sidebar.albums = 'ألبومات';
			window.locale.sidebar.radio = 'Radio';
	window.locale.sidebar.mystations = 'My Stations';
	window.locale.sidebar.catalog = 'Station Catalog';
		
		window.locale.ui.vkOwn = "My music";
		window.locale.ui.searchVK = "VKontakte البحث في";
		window.locale.sidebar.vk = "VK.COM";
		window.locale.radio.stationName = "New station name?";
	window.locale.radio.stationAddress = "New station address?";
}