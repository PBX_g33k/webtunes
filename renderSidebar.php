<?php
/* This will render a sidebar if you are user */
require('config.php');
if(!isset($_SESSION)) { session_start(); }
if(trim($_SESSION['uid']) == '') { $_SESSION['uid']='g';}
session_write_close();
function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}

if(isUser()) {
// Static part for library
	echo '<section id="sidebar">
<div class="scroll">
	<ul>
	<li><span class="category" onclick="toggleSect(\'libs-library\')"><localized key="library" category="sidebar" /></span>
	<ul id="libs-library">
	<li class="current lib" id="music"><a href="#" onclick="switchTo(\'music\')"><localized key="music" category="sidebar" /></a></li>
	<li class=" lib" id="compilation"><a href="#" onclick="switchTo(\'compilation\')"><localized key="compilations" category="sidebar" /></a></li>
	<li class=" lib" id="unsorted"><a href="#" onclick="switchTo(\'unsorted\')"><localized key="unsorted" category="sidebar" /></a></li>
	</ul>
	</li>
	</li>
		
';
echo '
	
	<li><span class="category" onclick="toggleSect(\'libs-radio\')"><localized key="radio" category="sidebar" /></span><span class="pladdbtn" onclick="newStation()">+</span>
	<ul id="libs-radio">
		<li class=" lib" id="my_radios"><a href="#" onclick="switchTo(\'my_radios\')"><localized key="mystations" category="sidebar" /></a></li>
		<li class=" lib" id="radios"><a href="#" onclick="switchTo(\'radios\')"><localized key="catalog" category="sidebar" /></a></li>
		<li class=" lib" id="digitally_imported"><a href="#" onclick="switchTo(\'digitally_imported\')">Digitally Imported</a></li>
	</ul>
	</li>
	</li>
		
';
	echo '<li><span class="category" onclick="toggleSect(\'playlists\')"><localized key="playlists" category="sidebar" /></span><span class="pladdbtn" onclick="mkLst()">+</span>
		<ul id="playlists">';
	// render playlists and make them draganddroppable
			$uid = mysql_escape_string($_SESSION['uid']);
			$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
			mysql_select_db($dbname, $dlink);
			mysql_query ("set character_set_client='utf8'", $dlink); 
			mysql_query ("set character_set_results='utf8'", $dlink); 
			mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
			$qu="SELECT * FROM playlists WHERE OWNER='".$uid."' ";
			$qu=$qu."ORDER BY id DESC";
			$result = mysql_query($qu, $dlink);
			$has = array();
			while($row = mysql_fetch_array($result)){
				array_push($has, $row['id']);
					$n = ($row['name']);
					
				if(mb_strlen($n, "UTF-8") > 16) { 
					$n = trim(mb_substr($n, 0, 15, "UTF-8"));
					$n .= '…';
				}
			   	echo '<li id="'.$row['id'].'" class="lib"';
			   	echo '  ondragover="allowDrop(event)" ondragend="noDrop(event)" ondragleave="noDrop(event)" ondrop="drop(event)" ><a href="#" onclick="switchTo(\''.$row['id'].'\')">'.$n.'</a></li>';			
			}
			if(!in_array($_GET['list'], $has) && trim($_GET['list']) != '') {
				$qu="SELECT * FROM playlists WHERE id='".intval($_GET['list'])."'";
				$qu=$qu."ORDER BY id DESC";
				$result = mysql_query($qu, $dlink);
				$row = mysql_fetch_array($result);
				echo '<li id="'.$row['id'].'" class="lib">';
			  if($row['owner'] != $_SESSION['uid'] && isUser()){ 	echo '<span class="savebutton" id="save'.$row['id'].'" onclick="copyList('.$row['id'].')">+</span>'; }
			   	echo '<a href="#" onclick="switchTo(\''.$row['id'].'\')">'.$row['name'].'</a></li>';	
			}


		
		echo '</ul>
	</li>';
	
	if(trim($sckey) != '' && trim($scsec) != '') {
		// static part for soundcloud
			echo '<li><span class="category" onclick="toggleSect(\'soundclouds\')"><localized key="soundcloud" category="sidebar" /></span><span class="pladdbtn" onclick="newSCList()">+</span>
		<ul id="soundclouds">';
			echo '<li id="sc_feed"   class="lib"><a href="#" onclick="switchTo(\'sc_feed\')"><localized key="cloudFeed" category="ui" /></a></li>';
			echo '<li id="sc_faves" ondragover="allowDrop(event)" ondragend="noDrop(event)" ondragleave="noDrop(event)" ondrop="dropSC(event)"  class="lib"><a href="#" onclick="switchTo(\'sc_faves\')"><localized key="cloudFavs" category="ui" /></a></li>';
			echo '<li id="sc_own"   class="lib"><a href="#" onclick="switchTo(\'sc_own\')"><localized key="cloudOwn" category="ui" /></a></li>';
			echo '<li id="sc_search"  style="" class="lib"><img src="img/search.png" class="search-icon"><a href="#" onclick="switchTo(\'sc_search\')"><localized key="cloudSearch" category="ui" /></a></li>';
			
			// dynamic part for soundcloud
			include 'scapi/Soundcloud.php';
			$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0); // connect mysql
			mysql_select_db($dbname, $dlink);
			mysql_query ("set character_set_client='utf8'", $dlink); 
			mysql_query ("set character_set_results='utf8'", $dlink); 
			mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
			
			$soundcloud = new Services_Soundcloud($sckey, $scsec, $webroot.'sc_auth.php'); //make new connection to SoundCloud
			
			$uid=mysql_escape_string($_SESSION['uid']); // User id
			if($uid != '' && $uid != 'g') {
				$at = mysql_fetch_array(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)); // find if user was connected

				$atok = unserialize($at['sc_token']); // if was connected, grab token
				
				if(trim($at['sc_token']) != '') {
					$soundcloud->setAccessToken($atok);
					$sets = objectToArray(json_decode($soundcloud->get('me/playlists'))); // Get playlist info
					foreach($sets as &$set) {
							echo '<li id="scl_'.$set['id'].'" ondragover="allowDrop(event)" ondragend="noDrop(event)" ondragleave="noDrop(event)" ondrop="dropSC(event)"  class="lib"';
							echo ' ><a href="#" onclick="switchTo(\'scl_'.$set['id'].'\')">'.$set['title'].'</a></li>';	
					}
				}

			}
				echo '</ul>
	</li>';
	}
	
	if(trim($vkkey) != '' && trim($vksec) != '') {
		// static part for soundcloud
			echo '<li><span class="category" onclick="toggleSect(\'vks\')"><localized key="vk" category="sidebar" /></span>
		<ul id="vks">';
			echo '<li id="vk_own"   class="lib"><a href="#" onclick="switchTo(\'vk_own\')"><localized key="vkOwn" category="ui" /></a></li>';
			echo '<li id="vk_search"  style="" class="lib"><img src="img/search.png" class="search-icon"><a href="#" onclick="switchTo(\'vk_search\')"><localized key="cloudSearch" category="ui" /></a></li>';
			
			echo '</ul>
	</li>';
	}
		
	
	// render album list
		echo '<li><span class="category" onclick="toggleSect(\'albums\')"><localized key="albums" category="sidebar" /></span>
		<ul id="albums">';
	
			$uid = mysql_escape_string($_SESSION['uid']);
			$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
			mysql_select_db($dbname, $dlink);
			mysql_query ("set character_set_client='utf8'", $dlink); 
			mysql_query ("set character_set_results='utf8'", $dlink); 
			mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
			$qu="SELECT * FROM albums WHERE OWNER='".$uid."'";
			$qu=$qu."ORDER BY name ASC";
			$result = mysql_query($qu, $dlink);
			while($row = mysql_fetch_array($result)){
				$n = $row['name'];
				if(strlen($n) > 17) { 
					$n = trim(substr($n, 0, 16));
					$n .= '…';
				}
			   	echo '<li id="alb'.$row['id'].'" class="lib"';
			   	echo ' ><a href="#" onclick="switchTo(\'alb'.$row['id'].'\')">'.$n.'</a></li>';			
			}

		
		echo '</ul>
	</li>';
echo'	</ul>
</div>
</section>';
}
?>