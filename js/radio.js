var stationName;
String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
function loadRadio(station) {
			console.log("Begin streaming radio station");
		
		$.ajax({type:"GET", url:'radio.php', contentType:"application/x-www-form-urlencoded", data:{"act":"url","station":station} , success:function(data) {
		var url = data;
			if(data.endsWith('/')) {
				 url = data+';'
			}
			loadRadioByUrl(station,url);
			}});
		
}

function loadRadioByUrl(station, url) {
window.audio = new Audio();
	window.audio.src =url; 
			window.audio.play();
			
			window.audio.addEventListener("stalled", function()
				  {
				  popMessage(window.locale.ui.bufferingStalled);
				  }
				);
			window.audio.addEventListener("canPlay", function()
				  {
					  hadstarted=true;
					document.getElementById('btn-pause').style.display="block";
					document.getElementById('btn-play').style.display="none";
					window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
					window.audio.play();
				  }
				);
				window.audio.addEventListener("ended", function(e)
				  {
				  	stop();
				  }
				);
				hadstarted=true;
				document.getElementById('btn-pause').style.display="block";
				document.getElementById('btn-play').style.display="none";
				window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
				$('#displ')[0].style.opacity="1";
				$( ".track-progress" ).seekslider('option','disabled',true);
				$( ".track-progress" )[0].style.opacity = "0";
				$( ".track-position-time" )[0].style.display = "none";
				$( ".track-position-remaining" )[0].style.display = "none";
				$( ".volume-bar" ).volslider('option','disabled', false);
				disableProg();
				stationName  =$('#RA'+station.toString()+'_title').html()
				document.getElementById('song-title').innerHTML=stationName;
				document.getElementById('song-album').innerHTML=stationName;
				document.getElementById('song-band').innerHTML="Radio "+stationName; 
				document.getElementById('sharer').style.display="none";
				document.getElementById('keeper').style.display="none";
				postMetadata(true, stationName, 'Radio', 'Radio');
				showAlbum();
				clearTimeout(displayBandTimeout);
				getMetas(station);
				for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
				for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
				try {
					document.getElementById('RA'+station.toString()+'_format').style.display="none";
					document.getElementById('RA'+station.toString()+'_ico').style.display="inline-block";
				} catch(e) {}
				
}

var metaWatchTimer = undefined;
var metaWatchData = undefined;

function getMetas(stid) {
	$.ajax({type:"GET", url:'radio.php',data:{'act':'meta', 'station':stid} , success:function(data) { 
		var song=data;
		if(song == metaWatchData) return;
		metaWatchData = song;
		metaWatchTimer = setTimeout(function(){ getMetas(stid); }, 5000);
		document.getElementById('song-title').innerHTML=song;
		postMetadata(true, song, stationName, 'Radio');
	}});
}


function copyStation(stid) {
	$.ajax({type:"GET", url:'radio.php',data:{'act':'copy', 'station':stid} , success:function(data) { 
		$('#RA'+stid.toString()+'_copy').remove();
		document.getElementById('my_radios').style.background = '#afa';
		setTimeout(function() {document.getElementById('my_radios').style.background = ''; }, 300);
		invalidateUI('my_radios');
		
	}});
}

function newStation() {
	var url=prompt(window.locale.radio.stationAddress, "");
		if (url == '' || typeof url == 'undefined') return;
	var name=prompt(window.locale.radio.stationName, "");
	if (name == '' || typeof name == 'undefined') return;
	$.ajax({type:"GET", url:'radio.php',data:{'act':'add', 'url':url, 'name':name} , success:function(data) { 
		document.getElementById('my_radios').style.background = '#afa';
		setTimeout(function() {document.getElementById('my_radios').style.background = ''; }, 300);
		invalidateUI('my_radios');
		
	}});
}

