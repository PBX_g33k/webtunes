# init.js
## performs the initialization of the player

This file contains only the jQuery document ready function.
It creates the volume and track sliders (with Jquery UI).
Then it checks whether the user was logged in and prompts to login, if no invite link nor a login was given.
After that it creates all the required form windows, such as the album addition form.
Afterwards it creates a drop-to-download control and adds a handler to it.
Finally, if the user is guest, it hides the sidebar space, makes the viewport take the whole window, and removes the search bar.