# player.js
## handles common playback routines

* `loadFile(url, notManual)` loads a file, whereas notManual is true when the load was triggered by software, such as the previous or next track skip, and not by the user. The `url` can be either a relative url to a file, such as `usertracks/0/nyancat.mp3` or an identifier such as `SC102935107` (actual identifier, try it out by typing `loadFile('SC102935107', true)` in the web debug console for some awesomeness! Please note you have to be authorized and connected to SoundCloud.)
* `buildQueue(url)` is the logical continuation of `loadFile` that builds a playback queue. The url is used to determine the current item.
* `playPause()` starts playback if it's paused and pauses if it's playing
* `stop()` stops playback and turns off the display
* `next()` and `prev()` move the queue pointer and start playback of the pointed-at item
* `loadCogs(url)` processes relative url tracks to load them with Aurora
* `seekHandler(ui)` is a handler of the seek slider