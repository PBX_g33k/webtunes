# dragdrop.js
## contains all the drag-and-drop routines

* `allowDrop(ev)` — makes a playlist in the sidebar allow dropping things into it and also highlights it
* `noDrop(ev)` — called when the user stops dragging into a playlist, unhighlights it
* `drag(ev)` — starts the drag operation, usually of a track
* `drop(ev)` — handles the drop, by calling 'mousedroppings.php' with a file name and list id
* `dropSC(ev)` — same as `drop`, but handles drops into SoundCloud Favorites
* `allowDropTrash(ev)` allows dropping stuff on trash
* `noDropTrash(ev)` handles trash drop cancel
* `dropToTrash(ev)` calls the UI-respective function in killitem.php to delete an item
