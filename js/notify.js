window.notifyInited = 0;
window.notificationLength = 2000;

function notify(title, text, image) {
  if (window.notifyInited == 1) {
    // 0 is PERMISSION_ALLOWED
    if(typeof image == 'undefined' || image == '') {
	    image = 'webtunes.png';
    }
    var notification  = window.webkitNotifications.createNotification(
      image,
      title,
    text
    );
    
    notification.show();
    setTimeout(function(){close_notification(notification);}, window.notificationLength);
  } 
}  
function close_notification(notif) {
	if(typeof notif != 'undefined') {
		notif.close();
	}
}
function init_notify() {
if(window.notifyInited == 1 || typeof window.webkitNotifications == 'undefined') return;
 var havePermission = window.webkitNotifications.checkPermission();
  if (havePermission == 0) {
    window.notifyInited = 1;
  } else {
      window.webkitNotifications.requestPermission(function() {
      	if(window.webkitNotifications.checkPermission() ==0) {
	      	window.notifyInited = 1;
      	}
      });
  }
}