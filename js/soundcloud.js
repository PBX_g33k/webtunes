function loadCloud(url, manually) {
		console.log("Aw shit its SC! Fall back to audio for the time being");
		window.audio = new Audio();
		var tid=url.substr(2, url.length);
	//	window.audio.src = "sc_auth.php?act=stream&tid="+tid; // load from stream proxier
	SC.get("/tracks/"+window.curUrl.substr(2, window.curUrl.length), {limit:1}, function(data) { 
	 		var addr = data.stream_url+"?consumer_key="+window.sckey;
	 			window.audio.src = addr;
	 		window.audio.play();
		window.audio.oncanplay=(function() {
			hadstarted=true;
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
		});
		window.prb = 0;
		window.audio.addEventListener("progress", function(a)
			  {
			  window.prb++;
			  document.getElementById('progressbar-footer').style.width=(window.audio.buffered.end(0) / window.audio.duration)*100+"%";
			  if((window.audio.buffered.end(0) / window.audio.duration)*100 >= 98) {
				 document.getElementById('footer-process').style.display="none"; 
			  } else {
				  document.getElementById('footer-process').style.display="block";
			  }
			  
			  document.getElementById('process-sig').innerHTML=window.locale.ui.cloudStream;
			  }
			);
		window.audio.addEventListener("stalled", function()
			  {
			  popMessage(window.locale.ui.bufferingStalled);
			  }
			);
		window.audio.addEventListener("canPlay", function()
			  {
				  hadstarted=true;
				document.getElementById('btn-pause').style.display="block";
				document.getElementById('btn-play').style.display="none";
				window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
				window.audio.play();
			  }
			);
			window.audio.addEventListener("ended", function(e)
			  {
			  	next();
			  }
			);
			window.audio.addEventListener("timeupdate", function(e)
			  {
			  	updateProg();
			  }
			);
			hadstarted=true;
			document.getElementById('btn-pause').style.display="block";
			document.getElementById('btn-play').style.display="none";
			window.audio.volume = $( ".volume-bar" ).volslider('option','value') / 100;
			$('#displ')[0].style.opacity="1";
			SC.get("/tracks/"+tid, {limit:1}, function(data) { 
			
			document.getElementById('song-title').innerHTML=data.title;
			document.title = window.locale.appname+': '+data.title+' - '+data.user.username;
			document.getElementById('song-band').innerHTML=data.user.username+window.locale.ui.onSC; // they asked me to credit them, so this is why
			postMetadata(true, data.title, data.user.username+window.locale.ui.onSC, window.locale.ui.soundCloudBeta);
			
			} );
			
			
			
				
				
			
				console.log('Fallback, enabling seek');
			$( ".track-progress" ).seekslider('option','disabled',false);
			$( ".volume-bar" ).volslider('option','disabled', false);
			disableProg();
			document.getElementById('song-album').innerHTML=window.locale.ui.soundCloudBeta;
			document.getElementById('sharer').style.display="table-cell";
			document.getElementById('keeper').style.display="table-cell";
			
			clearTimeout(displayBandTimeout);
			showBand();
			for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; }
			for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
			try {
				document.getElementById(url+'_format').style.display="none";
				document.getElementById(url+'_ico').style.display="inline-block";
			} catch(e) {}

	 	 } );
}

function moarSC(curs) {
	// load more soundcloud
	$('#SCmoar').empty();
	$('#SCmoar').remove();
	writeUI($('#listing').html(), 'sc_feed');
	//popMessage(window.locale.ui.wait);
	NProgress.start();
	$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"moar", cursor:curs} , success:function(data) {
	 			appendUI(data, 'sc_feed');
	 			NProgress.done();
			}});
}


function searchCloud () {
	// SoundCloud search handler
	$('#listing').empty();
	var q = $('#scsearchfield').val();
	var internalQ = q.replace(' ', '').replace('\'', '').replace('[','').replace(']','').replace('#','').replace('&','');
	var sd;
	if($('#scs_'+internalQ).length <= 0) {
		sd = $('#sc_search').clone(true);
		sd[0].style.display = "";
		sd[0].id = "scs_"+internalQ;
		var urq = q;
		if(urq.length > 16) {
			urq = q.substring(0, 15) + '…';
		}
		sd[0].innerHTML='<img src="img/search.png" class="search-icon"><a href="#" onclick="switchTo(\'scs_'+internalQ+'\')">'+urq+'</a>';
		$('#soundclouds').append(sd);
	} else {
		sd = $('#scs_'+internalQ);
	}
	
	
	switchTo(sd[0].id);
	NProgress.start();
	$.ajax({type:"GET", url:'sc_auth.php', contentType:"application/x-www-form-urlencoded", data:{"act":"search", "query": q} , success:function(data) {
				NProgress.done();
				writeUI(data, sd[0].id);
				
			}});
	$('#scsearchfield').val('');
}

function loadCloudList(lid) {
	// open playlist from SoundCloud
	$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"playlist", "lid":lid} ,   success:function(data) {
					writeUI(data, 'scl_'+lid);
					NProgress.done();
						}});
}

function loadCloudUI(itm) {
	 // open UI from SoundCloud
						document.getElementById('renamelist').style.display='none';
						document.getElementById('sharelist').style.display='none';
				$.ajax({type:"GET", url:'sc_auth.php', data:{"act":itm.substr(3, itm.length)} ,   success:function(data) {
					writeUI(data, itm);
					NProgress.done();
						}});
}
function newSCList() {

	var lname = prompt(window.locale.dialogs.newSClist, window.locale.dialogs.unamedSClist);
	if(lname == undefined) return;
	NProgress.start();
	$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"mklist", "name": lname} ,   success:function(data) {
							loadSidebar();
							//console.log(data);	
							NProgress.done();				
						}});
	
	
}