function popMessage(msg) { // show notification
	document.getElementById('popupt').innerHTML=msg;
	document.getElementById('popup').style.top='0';
	setTimeout(unPop,2000);
}
function unPop() { // hide notification
	document.getElementById('popup').style.top='-80px';
}

window.curUI = '';
var hasSwitched=false;
var displayBandTimeout=undefined;
var cachedUIs = new Object(); // Cache listings for snappier UI

function writeUI(data, identifier) {
	// This will write the UI to cache, and then present it in the viewport
	cachedUIs[identifier] = data;

	$('#listing').html(data);
	performNowPlayingIconRescan(window.curUrl);
}
function appendUI(data, identifier) {
	// this will append to cached ui and display
	cachedUIs[identifier] = cachedUIs[identifier] + data;
	if(window.curUI == identifier) {
		$('#listing').append(data);
		performNowPlayingIconRescan(window.curUrl);
	}
	
}

function invalidateUI(identifier) {
	// this will force a ui to reload next time it's called
	cachedUIs[identifier] = undefined;
	if(window.curUI == identifier) {
		switchTo(identifier);
	}
}

function useCachedUI(identifier) {
	// this will pop up a cached ui
	$('#listing').html(cachedUIs[identifier]);
	performNowPlayingIconRescan(window.curUrl);
}

function loadSidebar() { //ajax load sidebar
	$.ajax({type:"POST", url:'renderSidebar.php', success:function(data) {
				$('#sideplace').html(data);
				localizeSidebar();
				if(typeof window.starterUI != 'undefined') {
					processStarterUI('sidebar');
										return;
				}
				if(window.curUI != undefined) {
					hiliteSidebar(window.curUI);
				}
				
			}});

}

function showSect (sect) {
	
	$('#'+sect)[0].style.height = '';
	 $('#'+sect)[0].style.display = '';
	 $('#'+sect)[0].style.opacity = '1'; 
}
function hideSect (sect) {
	$('#'+sect)[0].style.opacity = '0'; 
	setTimeout(function() {$('#'+sect)[0].style.height = '0'; $('#'+sect)[0].style.display = 'none'; } , 500);
}
function toggleSect(sect) {
	if($('#'+sect)[0].style.display == 'none') {
		showSect (sect);
	} else {
		hideSect (sect);
	}
}

function hiliteSidebar(itm) {
		for(var a=0; a < $('.lib').length; a++) { $('.lib')[a].className='lib'; } // deselect all library items
	try {
		document.getElementById(itm).className='lib current'; // select the one we are presenting
	} catch(e) {
	
	}

}

function switchTo(itm) {
	// Present a view selected on sidebar
	if(itm == window.curUI && cachedUIs[itm] != undefined && !(itm.substr(0, 3) == 'scs' || itm.substr(0, 3) == 'vks')) {
			invalidateUI(itm);
			console.log("Forcing reload of "+itm);
			return;
	}
	if(window.curUI != itm) {
		hasSwitched=true;
		window.curUI=itm;
	}
	hiliteSidebar(itm);
	if(itm.substr(0, 2) == 'sc' || itm.substr(0, 2) == 'vk' || itm == 'music' || itm == 'unsorted' || itm == 'compilation' || itm=='radios' || itm == 'my_radios') {
		document.getElementById('renamelist').style.display='none'; // hide buttons on soundcloud unless playlist and on generic categories
		document.getElementById('sharelist').style.display='none';
	} else {
		document.getElementById('renamelist').style.display='inline-block';
		document.getElementById('sharelist').style.display='inline-block';
	}
	if(cachedUIs[itm] != undefined) {
		
		console.log("Using cached "+itm);
		useCachedUI(itm);
		return;
	}
	NProgress.start();
	if(itm.substring(0, 3) === 'alb') {
		showOneAlbum(itm.split('alb')[1]); // show album if it was album
		return;
	}

	
	if(itm.substr(0, 4) == "scs_") {
		// The search routine will handle this for us, and the next times the cache will do as well
		return;
	}
	if(itm.substr(0, 4) == "scl_") {
		loadCloudList(itm.substr(4, itm.length));
		
		return;
	}
	
	$('#listing').empty();
	
	switch (itm) {
			case 'sc_search': 
			if($('#ssw')[0].innerHTML != 'VK') {
					swSearch();
				} else {
					 $('#scsearchfield').focus();
				}
				NProgress.done();
			break;
			
			case 'vk_search': 
				if($('#ssw')[0].innerHTML != 'SC') {
					swSearch();
				} else {
					 $('#vksearchfield').focus();
				}
				NProgress.done();
			break;
			
		case "music": // download user music
		$.ajax({type:"POST", url:'getUserAlbums.php', contentType:"application/x-www-form-urlencoded", success:function(data) {
				writeUI(data, 'music');
				NProgress.done();
			}});
			break;
			
		case "compilation":
		$.ajax({type:"POST", url:'getUserAlbums.php', contentType:"application/x-www-form-urlencoded", data:{"compilations":"1"} , success:function(data) {
				writeUI(data, 'compilation');
				NProgress.done();
			}});
			break;
			
		case "unsorted":
			
			$.ajax({type:"POST", url:'getUserTracks.php', contentType:"application/x-www-form-urlencoded", data:{"user":"0"} , success:function(data) {
				writeUI(data, 'unsorted');
					NProgress.done();
			}});
			break;
			
		case "vk_own": 
			$.ajax({type:"GET", url:'vk_auth.php', contentType:"application/x-www-form-urlencoded", data:{"q":""} , success:function(data) {
				writeUI(data, 'vk_own');
					NProgress.done();
			}});
			break;
			
		case "radios":
			$.ajax({type:"GET", url:'radio.php', contentType:"application/x-www-form-urlencoded", data:{"mine":"0"} , success:function(data) {
				writeUI(data, 'radios');
					NProgress.done();
			}});
			break;
			
		case "my_radios":
			$.ajax({type:"GET", url:'radio.php', contentType:"application/x-www-form-urlencoded", data:{"mine":"1"} , success:function(data) {
				writeUI(data, 'my_radios');
					NProgress.done();
			}});
			break;
		
		case "digitally_imported":
			DI_LoadStations(itm);
			
			break;
			
			
		default:
			if(itm.substr(0, 3) != 'sc_') { // if plalist not soundcloud, use playlist display
				loadList(itm);
			} else {
				loadCloudUI(itm);
			}
			break;
	}


}

function performNowPlayingIconRescan(url) {
	
	for(var a=0; a < $('.now').length; a++) { $('.now')[a].style.display='none'; } // update icons
		for(var a=0; a < $('.format').length; a++) { $('.format')[a].style.display='inline-block'; }
		try {
			document.getElementById(url+'_format').style.display="none";
		document.getElementById(url+'_ico').style.display="inline-block";
		} catch(e) {}
}

function loadList(itm) {
	$.ajax({type:"POST", url:'getPls.php', contentType:"application/x-www-form-urlencoded",  data:{"user":"0", "list":itm} ,   success:function(data) {
				writeUI(data, itm);
				NProgress.done();
					if(window.starterUI !=itm) {
						document.getElementById('renamelist').style.display='inline-block';
						document.getElementById('sharelist').style.display='inline-block';
					} else {
						document.getElementById('renamelist').style.display='none';
						document.getElementById('sharelist').style.display='inline-block';
						$.ajax({type: "POST", url:'plsName.php', data: {'list':itm}, success:function(data) { 
						console.log('pls name '+data+' of '+itm);
							$('#playlists').append('<li id="'+itm.toString()+'" class="lib"><a href="#" onclick="switchTo(\''+itm.toString()+'\')">'+data+'<span class="savebutton" id="save'+itm.toString()+'" onclick="copyList('+itm.toString()+')">+</span></a></li>');
							hiliteSidebar(itm);
						}
						});
						window.starterUI = undefined;
					}
					
				if(window.starterTrack != undefined) { 
					loadFile($('.song')[window.starterTrack-1].id, false);
					window.starterTrack=undefined;
					}	
				}});
}

function showOneAlbum(id) { // Show a single album in listing
try {
	for(var a=0; a < $('.lib').length; a++) { $('.lib')[a].className='lib'; }
	document.getElementById('alb'+id).className='lib current';
} catch(e) {
	
}
	
	$('#listing').empty();
	hasSwitched=true;
		
			
			$.ajax({type:"POST", url:'getOneAlbum.php', data:{"album":id} , success:function(data) {
				writeUI(data, 'alb'+id);
				NProgress.done();
				if(window.starterTrack != undefined) { 
					loadFile($('.song')[window.starterTrack-1].id, false);
					window.starterTrack=undefined;
				}	
			}});


}

function showUser(itm) { // Show a user's unsorted in listing
	for(var a=0; a < $('.lib').length; a++) { $('.lib')[a].className='lib'; }
	$('#listing').empty();
	hasSwitched=true;
		
			
			$.ajax({type:"POST", url:'getUserTracksC.php', data:{"c":itm} , success:function(data) {
				writeUI('usr'+data);
				NProgress.done();
				if(window.starterTrack != undefined) { 
					loadFile($('.song')[window.starterTrack-1].id, false);
					window.starterTrack=undefined;
				}	
			}});


}

function showBand() { // animate to band line on display
	clearTimeout(displayBandTimeout);
	$('#song-album')[0].style.top="-13px";
	$('#song-album')[0].style.opacity="0";
	$('#song-band')[0].style.top="-13px";
	$('#song-band')[0].style.opacity="1";
	displayBandTimeout=setTimeout(showAlbum, 5000);
}
function showAlbum() { // animate to album line on display
	clearTimeout(displayBandTimeout);
	$('#song-album')[0].style.top="0px";
	$('#song-album')[0].style.opacity="1";
	$('#song-band')[0].style.top="0px";
	$('#song-band')[0].style.opacity="0";
	displayBandTimeout=setTimeout(showBand, 5000);
}

function disk() { // Disk space refreshener
	if(window.uid != 'a') return;
	$('#disk').empty();
	$.ajax({type:"POST", url:'diskspace.php', success:function(data) {
								$('#disk').html(window.locale.ui.diskSpace + data);
					}});

}

 
function about() {
	//invalidateUI('about');
	$.ajax({type:"GET", url:'about.php', success:function(data) {
							NProgress.done();
								writeUI(data+window.locale.credit, 'about');
						
					}});
	
}


function changeLang() {
	if(isLoadLocaleInProgress) return false;
	$.ajax({type:"GET", url:'setLang.php', data:{"loc":$('#langpicker').val()}});
	loadLocale($('#langpicker').val());
	return true;
}


function updateProg() {
	// progress update
if(window.player == undefined && window.audio == undefined) return;
	if(window.player != undefined) {
		$( ".track-progress" ).seekslider('option','value',(window.player.currentTime / window.player.duration)*1000);
		var cseconds = window.player.currentTime / 1000;
		var cnumminutes = Math.floor((((cseconds % 31536000) % 86400) % 3600) / 60);
		var csminutes = cnumminutes.toString();
		if(cnumminutes < 10) { csminutes = '0'+csminutes; }
		var cnumseconds = Math.floor((((cseconds % 31536000) % 86400) % 3600) % 60);
		var csseconds = cnumseconds.toString();
		if(cnumseconds < 10) { csseconds = '0'+csseconds; }
		$('.track-position-time')[0].innerHTML = csminutes.toString()+':'+csseconds.toString();
		
		var aseconds = (window.player.duration-window.player.currentTime) / 1000;
		var anumminutes = Math.floor((((aseconds % 31536000) % 86400) % 3600) / 60);
		var asminutes = anumminutes.toString();
		if(anumminutes < 10) { asminutes = '0'+asminutes; }
		var anumseconds = Math.floor((((aseconds % 31536000) % 86400) % 3600) % 60);
		var asseconds = anumseconds.toString();
		if(anumseconds < 10) { asseconds = '0'+asseconds; }
		$('.track-position-remaining')[0].innerHTML = '-'+asminutes.toString()+':'+asseconds.toString();
		if(window.player.buffered == 100 && window.player.duration - window.player.currentTime < 1200) { clearTimeout(samplesFix); samplesFix=setTimeout(next, 1000); }
		cseconds=0; aseconds=0;
	}
	if(window.audio != undefined) {
		$( ".track-progress" ).seekslider('option','value',(window.audio.currentTime / window.audio.duration)*1000);
		var cseconds = window.audio.currentTime;
		var cnumminutes = Math.floor((((cseconds % 31536000) % 86400) % 3600) / 60);
		var csminutes = cnumminutes.toString();
		if(cnumminutes < 10) { csminutes = '0'+csminutes; }
		var cnumseconds = Math.floor((((cseconds % 31536000) % 86400) % 3600) % 60);
		var csseconds = cnumseconds.toString();
		if(cnumseconds < 10) { csseconds = '0'+csseconds; }
		$('.track-position-time')[0].innerHTML = csminutes.toString()+':'+csseconds.toString();
		
		var aseconds = (window.audio.duration-window.audio.currentTime);
		var anumminutes = Math.floor((((aseconds % 31536000) % 86400) % 3600) / 60);
		var asminutes = anumminutes.toString();
		if(anumminutes < 10) { asminutes = '0'+asminutes; }
		var anumseconds = Math.floor((((aseconds % 31536000) % 86400) % 3600) % 60);
		var asseconds = anumseconds.toString();
		if(anumseconds < 10) { asseconds = '0'+asseconds; }
		$('.track-position-remaining')[0].innerHTML = '-'+asminutes.toString()+':'+asseconds.toString();
		cseconds=0; aseconds=0;
	}
}

function disableProg() {
	$( ".track-progress" ).seekslider('option','value',0);
	$('.track-position-time')[0].innerHTML = "--:--";
	$('.track-position-remaining')[0].innerHTML = "--:--";
}
function swSearch() {
 if($('#ssw')[0].innerHTML == 'VK') {
	  $('#scsearchfield')[0].style.left = "-100%";
	  $('#vksearchfield')[0].style.left = "44px"; 
	  $('#ssw')[0].innerHTML = 'SC'; 
  } else {
	  $('#scsearchfield')[0].style.left = "44px";
	  $('#vksearchfield')[0].style.left = "100%";
	   $('#ssw')[0].innerHTML = 'VK';
   } 
  }
