<?php

function genCell($identifier, $isDraggable, $iterator, $title, $time, $author, $album, $format, $accessory) {
	echo "<li class=\"song\"  id=\"".$identifier."\"";
	if($isDraggable) {
		echo " draggable=\"true\" ondragstart=\"drag(event)\"";
	}
	echo "  onclick=\"loadFile('$identifier')\">";
			echo '<span class="song-number"><span class="format" id="'.$identifier.'_format">'.$format.'</span><img src="img/now.png" class="now" id="'.$identifier.'_ico"></i> '.$iterator.'</span>'.$accessory.'
					<span class="song-name" id="'.$identifier.'_title">'.$title.'</span>
					<span class="song-time">'.$time.'</span>
					<span class="song-artist" id="'.$stream_id.'_author">'.$author.'</span>
					<span class="song-album">'.$album.'</span>';
			echo "</li>";
}
function msToTime($ms) {
$input = $ms;
	$uSec = $input % 1000;
$input = floor($input / 1000);

$seconds = $input % 60;
$input = floor($input / 60);

$minutes = $input % 60;
$input = floor($input / 60); 
$outstr = '';
if($minutes < 10) {
	$outstr .= '0'.$minutes;
} else {
	$outstr .= $minutes;
}
$outstr.= ':';
if($seconds < 10) {
	$outstr .= '0'.$seconds;
} else {
	$outstr .= $seconds;
}
return $outstr;

}
function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}
?>