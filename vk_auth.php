<?php

/**
 * Example 2.
 * Get access token via OAuth and usage VK API.
 * @link http://vk.com/developers.php VK API
 */


require_once('VK/VK.php');
require_once('VK/VKException.php');
if(!isset($_SESSION)){ session_start(); }
require('config.php');
require_once('tableview.utils.php');

if(!isUser()) {
	die('Not a user.');
}
if(trim($_GET['act']) != '') { /* recover action JIC */
	$action = mysql_escape_string($_GET['act']);
	$_SESSION['lact'] = $action;
} 
session_write_close();
		$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0); // connect mysql
			mysql_select_db($dbname, $dlink);
			mysql_query ("set character_set_client='utf8'", $dlink); 
			mysql_query ("set character_set_results='utf8'", $dlink); 
			mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 

$vk_config = array(
    'app_id'        => $vkkey,
    'api_secret'    => $vksec,
    'callback_url'  => $webroot.'vk_auth.php',
    'api_settings'  => 'audio,offline' // In this example use 'friends'.
    // If you need infinite token use key 'offline'.
);


$uid=mysql_escape_string($_SESSION['uid']); // User id
$at = mysql_fetch_array(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)); // find if user was connected
if(trim($at['vk_token']) != '') {
	$access_token = unserialize($at['vk_token']);
} else {
	$codique = $_GET['code'];
}

try {
   

	    
    if (trim($codique) == '' && !isset($access_token)) {
     $vk = new VK\VK($vk_config['app_id'], $vk_config['api_secret']);
        /**
         * If you need switch the application in test mode,
         * add another parameter "true". Default value "false".
         * Ex. $vk->getAuthorizeURL($api_settings, $callback_url, true);
         */
        $authorize_url = $vk->getAuthorizeURL(
            $vk_config['api_settings'], $vk_config['callback_url'], true);
            
        echo '<a href="' . $authorize_url . '" target="_blank">Sign in with VK</a>';
    } else {
    	
    	if(!isset($access_token) ) {
    		
	    	 $vk = new VK\VK($vk_config['app_id'], $vk_config['api_secret']);
	    	$access_token = $vk->getAccessToken($_REQUEST['code'], $vk_config['callback_url']);
	    	$svt = serialize($access_token);
	        if(mysql_num_rows(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)) > 0) {
				mysql_query("UPDATE cloud SET vk_token='$svt' WHERE uid='$uid'", $dlink);
			} else {
				mysql_query("INSERT INTO cloud (uid, vk_token) VALUES ('$uid', '$svt')", $dlink);
			}
			die("Now close this tab and reuse VK in WebTunes again");
    	} else {
	    	 $vk = new VK\VK($vk_config['app_id'], $vk_config['api_secret'], $access_token['access_token']);
    	}
        
        /* echo 'access token: ' . $access_token['access_token']
            . '<br />expires: ' . $access_token['expires_in'] . ' sec.'
            . '<br />user id: ' . $access_token['user_id'] . '<br /><br />';
            */
       $q  = trim($_GET['q'])!='' ? $_GET['q'] : '';
       if(trim($q) != '') 
       {
	       $rs = $vk->api('audio.search', array(
                'v' => '2.0',
                'count' => '9999999',
                'q' => $q
            ));

       } else {
	       $rs = $vk->api('audio.get', array(
                'v' => '2.0',
                'count' => '9999999'
            ));

       }
       
                 $iterator=0;
       $tracks = $rs['response'];
       
       foreach($tracks as &$track) {
      	
      	if(isset($track['aid'])) {
      	$iterator = $iterator+1;
	      	 genCell('VK'.$track['owner_id'].'_'.$track['aid'], true, $iterator, $track['title'], msToTime(($track['duration']) * 1000), $track['artist'], '', "VK");
      	}
	       
       }
    }

   
  
   
  } catch (VK\VKException $error) {
    echo $error->getMessage();
}
