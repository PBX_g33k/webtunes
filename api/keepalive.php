<?
if(!isset($_SESSION)) {
	session_start();
}
if($_SESSION['hasLoggedIn'] == 1) {
	echo(json_encode(Array('code'=>'success')));
} else {
	echo(json_encode(Array('code'=>'failure', 'detail'=>'notLoggedIn')));
}