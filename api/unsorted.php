<?
require('../config.php');

// include getID3() library (can be in a different directory if full path is specified)
require_once('../getid3/getid3.php');
if(!isUser()) { die(json_encode(array('code'=>'failure', 'reason'=>'notLoggedIn')));}
session_start();
session_write_close();
// Initialize getID3 engine
$getID3 = new getID3;

$DirectoryToScan = '../usertracks/'.intval($_SESSION['uid']); // change to whatever directory you want to scan
$dir = opendir($DirectoryToScan);
$iterator = 1;
while ($files[] = readdir($dir));
sort($files);
closedir($dir);
$items = array();
foreach ($files as $file) {
	$FullFileName = realpath($DirectoryToScan.'/'.$file);
	if ((substr($FullFileName, 0, 1) != '.') && is_file($FullFileName)) {
		set_time_limit(30);

		$ThisFileInfo = $getID3->analyze($FullFileName);

		getid3_lib::CopyTagsToComments($ThisFileInfo);
		
		$fname=$ThisFileInfo['filename'];
		$artist=$ThisFileInfo['comments_html']['artist'][0];
		$title = $ThisFileInfo['comments_html']['title'][0];
		$album = $ThisFileInfo['comments_html']['album'][0];
		$time=$ThisFileInfo['playtime_string'];
		if(trim($title) == '') { $title = $fname; }
		
		array_push($items, array(
			'fname' => 'usertracks/'.intval($_SESSION['uid']).'/'.$fname,
			'artist' => $artist,
			'title' => $title,
			'album' => $album,
			'time' => $time		
		));
		
		}
}

echo json_encode(array('code'=>'ok', 'result'=>$items));

?>