<?php

if(!isset($_SESSION)) {
	session_start();
}
require('config.php');
if(!isUser()) { die('SESSION_NOT_EXIST');}
$_SESSION['keepAlive'] = (time());
session_write_close();
die('SESSION_OK');
?>