<?php
/* Despite the name, it's a script to handle drag and drop into a playlist */
	require('config.php');
	require_once('tableview.utils.php');
require_once('VK/VK.php');
require_once('VK/VKException.php');
	// include getID3() librardy (can be in a different directory if full path is specified)
		require_once('getid3/getid3.php');
	if(!isUser()) { die('disallow');}
	include 'scapi/Soundcloud.php';
	
		
		// Initialize getID3 engine
		$getID3 = new getID3;
	$uid = intval($_SESSION['uid']);
	$pls = intval($_POST['list']);
	$fname = mysql_escape_string(urldecode($_POST['file']));
	$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
	mysql_select_db($dbname, $dlink);
	mysql_query ("set character_set_client='utf8'", $dlink); 
	mysql_query ("set character_set_results='utf8'", $dlink); 
	mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
	//echo $fname;
	$original = mysql_query("SELECT * FROM playlists WHERE id='".$pls."'", $dlink);
	$content = mysql_fetch_array($original);
	if($content['owner'] != $uid) {
		die('disallow'); // No edit others playlists
	}

	$plist = json_decode($content['contents']);
	if(!isset($plist) || trim($content['contents']) == '' || !is_array($plist)) {
		$plist=Array();
	}

	if(mb_substr($fname, 0, 2) == 'VK') {
		
		// Cache and insert VK track
		$vk_config = array(
		    'app_id'        => $vkkey,
		    'api_secret'    => $vksec,
		    'callback_url'  => $webroot.'vk_auth.php',
		    'api_settings'  => 'audio,offline' 
		);
		
		
		$uid=mysql_escape_string($_SESSION['uid']); // User id
		$at = mysql_fetch_array(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)); // find if user was connected
		if(trim($at['vk_token']) != '') {
			$access_token = unserialize($at['vk_token']);
		} else {
			$codique = $_GET['code'];
		}
	
		$vk = new VK\VK($vk_config['app_id'], $vk_config['api_secret'], $access_token['access_token']);
		 $rs = $vk->api('audio.getById', array(
                'v' => '2.0',
                'audios' => substr($fname, 2, strlen($fname))
            ));
            
            $track = $rs["response"][0];
           
            $trk = array('fname'=>$fname, 
            			 'artist' => ($track['artist']),
            			 'title' => ($track['title']),
            			 'album' => '',
            			 'time' => msToTime(($track['duration']) * 1000),
            			 'ext'=>'VK');
		
	} else if (mb_substr($fname,0,2) == 'SC') {
		// Cache and insert SC track
		$soundcloud = new Services_Soundcloud($sckey, $scsec, $webroot.'sc_auth.php'); //make new connection to SoundCloud
		$uid=mysql_escape_string($_SESSION['uid']); // User id
		$at = mysql_fetch_array(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)); // find if user was connected
		
		$atok = unserialize($at['sc_token']); // if was connected, grab token

		if($atok != '') { // We had a token saved, load it
			$soundcloud->setAccessToken($atok);
		}else { die('fail: no token SC');}
		$tid  = substr($fname, 2, strlen($fname));
		$track_datasa = objectToArray(json_decode($soundcloud->get('tracks/'.$tid))); // Get track info
			$t = $track_datasa;
			$trk = array('fname'=>$fname, 
            			 'artist' => ($t['user']['username']),
            			 'title' => ($t['title']),
            			 'album' => '',
            			 'time' => msToTime(($t['duration'])),
            			 'ext'=>'SC');
	} else {
		// Cache and insert simple track
		set_time_limit(30);
	
			$ThisFileInfo = $getID3->analyze(stripcslashes($fname));
	
			getid3_lib::CopyTagsToComments($ThisFileInfo);
			
			//$fname=$ThisFileInfo['filename'];
			$artist=$ThisFileInfo['comments_html']['artist'][0];
			$title = $ThisFileInfo['comments_html']['title'][0];
			$album = $ThisFileInfo['comments_html']['album'][0];
			$ext =strtoupper(end( explode('.', $fname)));
		
			$time=$ThisFileInfo['playtime_string'];
			if(trim($title) == '') { $title = basename($fname); }
		
			 $trk = array('fname' => $fname,
			 				'artist' => ($artist),
			 				'title'=>($title),
			 				'album'=>($album),
			 				'time'=>$time,
			 				'ext'=>($ext));
			 				
	}
	$trk['fname'] = str_replace('\\\'', '\'', $trk['fname']);
	$trk['title'] = str_replace('\\\'', '\'', $trk['title']);
	array_push($plist, ($trk));
	mysql_query("UPDATE playlists SET contents='".mysql_escape_string(json_encode($plist))."' WHERE id='".$pls."'");
	echo 'done';
	mysql_close($dlink);


?>