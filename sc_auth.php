<?php
/* The Migthy SoundCloud(R) Connector */

if(!isset($_SESSION)){ session_start(); }

require('config.php');
include 'scapi/Soundcloud.php';
if(!isUser()) {
  die('Not a user.');
}

if(trim($_GET['act']) != '') { /* recover action JIC */
  $action = mysql_escape_string($_GET['act']);
  $_SESSION['lact'] = $action;
} 
session_write_close();

$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0); // connect mysql
mysql_select_db($dbname, $dlink);
mysql_query ("set character_set_client='utf8'", $dlink); 
mysql_query ("set character_set_results='utf8'", $dlink); 
mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 

$soundcloud = new Services_Soundcloud($sckey, $scsec, $webroot.'sc_auth.php'); //make new connection to SoundCloud
$codique = $_GET['code']; // Auth code if present
$uid=mysql_escape_string($_SESSION['uid']); // User id
$at = mysql_fetch_array(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)); // find if user was connected

$atok = unserialize($at['sc_token']); // if was connected, grab token

require_once('tableview.utils.php');

try { // if failing, falls back to connect to soundcloud button
  if(trim($_SESSION['lact']) != '') { // Had last action restored
    $action = $_SESSION['lact'];
    $_SESSION['lact'] = '';
  }
  if($codique != '') { // We were connecting, actually
    $accessToken = $soundcloud->accessToken($codique);
    $soundcloud->setAccessToken($accessToken["access_token"]);
    // Write down the token (hopefully non-expiring)
    $acctok = serialize($accessToken["access_token"]);
    if(mysql_num_rows(mysql_query("SELECT * FROM cloud WHERE uid='$uid'", $dlink)) > 0) {
      mysql_query("UPDATE cloud SET sc_token='$acctok' WHERE uid='$uid'", $dlink);
    } else {
      mysql_query("INSERT INTO cloud (uid, sc_token) VALUES ('$uid', '$acctok')", $dlink);
    }

    die("Now close this tab and click SoundCloud in WebTunes again");
  }
  if($atok != '') { // We had a token saved, load it
    $soundcloud->setAccessToken($atok);
  }
  if($action == 'feed' || $action == '') { // No action specified, defaults to feed
    $feed = json_decode($soundcloud->get('me/activities'), true); // Load feed
    $nextpar = ($feed['next_href']); // Get cursor
    $nextpar = str_replace('https://api.soundcloud.com/me/activities?cursor=', '', $nextpar); // Extract cursor from link
    $tracks = $feed['collection']; // Tracks
    foreach($tracks as &$track) {
      if($track['type'] == 'track') { // If actually track

        $t = $track['origin'];
        $stream_id = $t['id'];
        $title = $t['title'];
        $time = msToTime($t['duration']);
        $author = $t['user']['username']; // Get data and output as row
        genCell('SC'.$stream_id, true, $iterator, $title, $time, $author, $album, "SC");

      }
    }
    echo "<li class=\"song\"  id=\"SCmoar\" draggable=\"false\"  onclick=\"moarSC('$nextpar')\">";
    echo '<span><center>Click me to load more</center></span>';
    echo "</li>";
  }
  if($action == 'own') {
    $feed = json_decode($soundcloud->get('me/tracks'), true); // Load own tracks

    foreach($feed as &$track) {
      if($track['kind'] == 'track') {
        $t = $track;
        $stream_id = $t['id'];
        $title = $t['title'];
        $time = msToTime($t['duration']);
        $author = $t['user']['username']; // output each as row
        genCell('SC'.$stream_id, true, $iterator, $title, $time, $author, $album, "SC");
      }
    }

  }
  if($action == 'moar') { // Load more by cursor
    $param = $_GET['cursor'];
    $feed = json_decode($soundcloud->get('me/activities', array('cursor'=> $param)), true); // get datas
    $nextpar = ($feed['next_href']);

    $nextpar = str_replace('https://api.soundcloud.com/me/activities?cursor=', '', $nextpar);
    $tracks = $feed['collection'];
    foreach($tracks as &$track) { // output each row
      if($track['type'] == 'track') {

        $t = $track['origin'];
        $stream_id = $t['id'];
        $title = $t['title']; $time = msToTime($t['duration']);
        $author = $t['user']['username'];
        genCell('SC'.$stream_id, true, $iterator, $title, $time, $author, $album, "SC");
      }
    }
    echo "<li class=\"song\"  id=\"SCmoar\" draggable=\"false\"  onclick=\"moarSC('$nextpar')\">";
    echo '<span><center>Click me to load more </center></span>';
    echo "</li>";
  }
  if($action == 'faves') {
    // list favorites
    $feed = json_decode($soundcloud->get('me/favorites', array('limit' => 1000000)), true); // limit thing due to a bug
    $iterator=0;
    foreach($feed as &$track) { // list all
      ++$iterator;
      $t = $track;
      $stream_id = $t['id'];
      $wf=$t['waveform_url'];
      $title = $t['title']; $time = msToTime($t['duration']);
      $author = $t['user']['username'];
      genCell('SC'.$stream_id, true, $iterator, $title, $time, $author, $album, "SC");
    }
  }
  if($action == 'playlist') {
    // list a list
    $par = $_GET['lid'];
    $list = objectToArray(json_decode($soundcloud->get('playlists/'.$par, array('limit' => 1000000)), true)); // limit thing due to a bug
    $iterator=0;
    $tracks = $list['tracks'];
    //var_dump($tracks); die();
    if(count($tracks) == 0) {
      die('<span class="gomennasai">Sorry, empty list</span>');
    }
    foreach($tracks as &$track) { // list all

      ++$iterator;
      $t = $track;
      $stream_id = $t['id'];
      $wf=$t['waveform_url'];
      $title = $t['title']; $time = msToTime($t['duration']);
      $author = $t['user']['username'];
      genCell('SC'.$stream_id, true, $iterator, $title, $time, $author, $album, "SC");
    }
  }

  if($action == 'search') {
    $par = $_GET['query'];

    if(trim($par) != '') {
      $tracks = json_decode($soundcloud->get('tracks', array('q' => $par, 'limit'=>1000000)));
      if(count($tracks) == 0) {
        die('<span class="gomennasai">Sorry, not found</span><br><span class="sad">:(</span>');
      }
      foreach($tracks as &$track) { // output each row
      
      $t = objectToArray($track);

        if($t['kind'] == 'track') {
          $stream_id = $t['id'];
          $title = $t['title']; $time = msToTime($t['duration']);
          $author = $t['user']['username'];
          genCell('SC'.$stream_id, true, $iterator, $title, $time, $author, $album, "SC");

        }

      }

    }
  }

  if($action == 'stream_down') { // download stream as file
    $par = $_GET['tid'];
    $track_datasa = objectToArray(json_decode($soundcloud->get('tracks/'.$par))); // Get track info
    header('Content-Description: SoundCloud to WebTunes'); // Present ourselves as octet stream
    header('Content-Type: application/octet-stream'); 
    if($track_datasa['downloadable']) { // The track has downloading capability
      header('Content-Disposition: attachment; filename=SoundCloud_Download_'.$par.'.'.$track_datasa['original_format']); // Get original file name and ext
      header('Content-Length: ' . $track_datasa['original_content_size']);
    } else {
      header('Content-Disposition: attachment; filename=SoundCloud_Stream_'.$par.'.mp3'); // Erm, rip the stream whatsoever
    }
    header('Content-Transfer-Encoding: binary'); //Other headers
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    
    ob_clean();
    flush();
    if(connection_aborted() == 1) {
      ob_end_flush();
      flush();
      die();
    }

    if($track_datasa['downloadable']) {
      // This method was altered too, it will echo right away
      $soundcloud->download($par);
    } else {
      // Rip the stream if no download there
      $soundcloud->stream($par);
    }
    if(connection_aborted() == 1) {
      ob_end_flush();
      flush();
      die();
    }				
  }

  if($action == 'drop') { // handle drag and drop on favorites

    $par = $_GET['tid'];
    $dest = $_GET['dest'];

    if($dest == 'faves') {
      $soundcloud->put('me/favorites/'.$par) or die('couldnt fav track');
    } else {
      $list = objectToArray(json_decode($soundcloud->get('playlists/'.$dest, array('limit' => 1000000)), true)); // limit thing due to a bug
      $newlist = array();

      $tracks = $list['tracks'];
      array_push($newlist, $par);
      foreach($tracks as &$track) { // list all
        array_push($newlist, $track['id']);
      }
      $soundcloud->updatePlaylist($dest, $newlist) or die('couldnt update playlist');
    }

    die("done");
  }

  if($action == 'trash') {
    $from = $_GET['from']; 
    $what = $_GET['tid' ];

    if($from == 'faves') {

      $soundcloud->delete('me/favorites/'.$what) or die('could not unfav track');
    } else {
      $list = objectToArray(json_decode($soundcloud->get('playlists/'.$from, array('limit' => 1000000)), true)); // limit thing due to a bug
      $newlist = array();

      $tracks = $list['tracks'];
      foreach($tracks as &$track) { // list all
        //echo "Checking ".$track['id']." \n";
        if($track['id'] != trim($what)) {
          //echo $track['id']." != ".$what."\n";
          array_push($newlist, $track['id']);
        }
      }

      $soundcloud->updatePlaylist($from, $newlist) or die('could not update playlist');
    }
   
    die('trashed');
  }

  if($action == 'mklist') {
    $lname = $_GET['name'];
    $playlist  = "playlist[title]=$lname";
    // create the playlist
    $response = objectToArray(json_decode($soundcloud->post('playlists', $playlist)));

    die($response['id']);
  }

  if($action == 'killlist') {
    $lid = $_GET['lid'];

    $response = objectToArray(json_decode($soundcloud->delete('playlists/'.$lid, $playlist)));
    echo $response['status'];
    die();
  }

  if($action == 'mklink') {
    $tid = $_GET['id'];
    if($_GET['kind'] == 'trk') {
      $response = objectToArray(json_decode($soundcloud->get('tracks/'.$tid)));
    } else {
      $response = objectToArray(json_decode($soundcloud->get('playlists/'.$tid)));
    }
    die($response["permalink_url"]);
  }


} catch (Services_Soundcloud_Invalid_Http_Response_Code_Exception $e) { // Error -- show connect button
  echo '<a href="' . $soundcloud->getAuthorizeUrl() . '" target="_blank" id="sca" onclick="document.getElementById(\'sca\').innerHTML=\'\'; var c=window.curUI+\'\'; switchTo(\'\'); invalidateUI(c)"><img src="http://connect.soundcloud.com/2/btn-connect-sc-l.png" alt="Connect to SoundCloud"></a><br />Click the respective tab in the sidebar after authorization again in order to access SoundCloud<br />This may also have been caused by a communication error, if the code below is not 401, no need to reconnect.';
  exit($e->getMessage());

}

?>

